import Route from "@ioc:Adonis/Core/Route";
import Logger from "@ioc:Adonis/Core/Logger";
import HealthCheck from "@ioc:Adonis/Core/HealthCheck";


Route.resource("interns", "InternsController").apiOnly();

Route.get("/cohort", async (ctx) => {
  const { default: CohortsController } = await import(
    "App/Controllers/Http/CohortsController"
  );
  Logger.info("Querry");
  return new CohortsController().index(ctx);
  
});

Route.post("register", async (ctx) => {
  const { default: AuthController } = await import(
    "App/Controllers/Http/AuthController"
  );
  return new AuthController().register(ctx);
});

Route.post("login", async (ctx) => {
    const { default: AuthController } = await import(
      "App/Controllers/Http/AuthController"
    );
    return new AuthController().login(ctx);
});


Route.post("/logout", async (ctx) => {
  const { default: AuthController } = await import(
    "App/Controllers/Http/AuthController"
  );
  return new AuthController().logout(ctx);
});

Route.get("health", async ({ response }) => {
  const report = await HealthCheck.getReport();

  return report.healthy ? response.ok(report) : response.badRequest(report);
});
