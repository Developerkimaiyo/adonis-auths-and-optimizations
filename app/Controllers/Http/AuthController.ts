import Auth from "App/Models/Auth";
import {
  schema,
  rules, // 👈 import validator
} from "@ioc:Adonis/Core/Validator";

export default class AuthController {
  public async register(ctx) {
    const data = ctx.request.only(["email", "password"]);
    try {
      await ctx.request.validate({
        schema: schema.create({
          email: schema.string({}, [
            rules.email(),
            rules.unique({
              table: "auths",
              column: "email",
              caseInsensitive: true,
            }),
          ]),
          password: schema.string({}, [
            rules.minLength(6),
            rules.maxLength(32),
            rules.required(),
          ]),
        }),
        messages: {
          required: "The {{ field }} is required to create a new account",
          "email.unique": "email already exists",
          "password.minLength":
            "The passwordshould have a minimum of 6 characters",
        },
      });

      const user = await new Auth();
      user.email = data.email;
      user.password = data.password;
      await user.save();
      const token = await ctx.auth
        .use("api")
        .attempt(data.email, data.password);
      return token;
    } catch (error) {
      
      return ctx.response.badRequest(error.messages.errors);
    }
  }

  public async login(ctx) {
    const email = ctx.request.input("email");
    const password = ctx.request.input("password");

    try {
      const token = await ctx.auth.use("api").attempt(email, password);
      return token;
    } catch {
      return ctx.response.badRequest({
        status: "error",
        message: "Invalid email/password",
      });
    }
  }
  public async logout(ctx) {
    await ctx.auth.use("api").revoke();
    return {
      revoked: true,
    };
  }
}
