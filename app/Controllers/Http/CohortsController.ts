import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { ModelPaginatorContract } from '@ioc:Adonis/Lucid/Model';


import Intern from "App/Models/Intern";

export default class CohortsController {
  public async index(
    ctx: HttpContextContract
  ): Promise<ModelPaginatorContract<Intern>> {
    const { page = 1, ...input } = ctx.request.qs();

    return Intern.filter(input).preload("stacks").paginate(page, 15);
  }
}
