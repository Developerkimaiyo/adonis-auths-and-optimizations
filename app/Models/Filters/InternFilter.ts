import { BaseModelFilter } from '@ioc:Adonis/Addons/LucidFilter'
import { ModelQueryBuilderContract } from '@ioc:Adonis/Lucid/Orm'
import Interns from 'App/Models/Intern'

export default class InternFilter extends BaseModelFilter {
  public $query: ModelQueryBuilderContract<typeof Interns, Interns>;

  public static blacklist: string[] = ["secretMethod"];

  id(id: number) {
    this.$query.where("id", id);
  }

  name(name: string) {
    this.$query.where((builder) => {
      builder.where("name", "LIKE", `%${name}%`);
    });
  }
  email(email: string) {
    this.$query.where("email", "LIKE", `${email}%`);
  }

  month(month: string) {
    this.$query.where("month", "LIKE", `${month}%`);
  }


  year(year: number) {
    this.$query.where("year", year);
  }
}
